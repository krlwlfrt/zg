const fileInput = document.getElementById('file') as HTMLInputElement;
const canvas = document.getElementById('canvas') as HTMLCanvasElement;
const progressBar = document.getElementById('progress');
const context = canvas?.getContext('2d');
const fileReader = new FileReader();
let target: number[][] = [];
let image: HTMLImageElement;

fileReader.onprogress = (progress) => {
  console.log(progress);
};

canvas.onclick = (e) => {
  // eslint-disable-next-line no-magic-numbers
  if (target.length === 2) {
    drawImage();
    target = [];
  }

  target.push([e.offsetX, e.offsetY]);

  // eslint-disable-next-line no-magic-numbers
  if (target.length === 2) {
    context?.beginPath();
    // eslint-disable-next-line no-magic-numbers
    context?.rect(target[0][0], target[0][1], target[1][0] - target[0][0], target[1][1] - target[0][1]);
    context?.stroke();
  }
};

fileInput.onchange = fileSelected;

/**
 * Handler when file is selected
 */
function fileSelected() {
  if (fileInput?.files instanceof FileList && fileInput?.files.length > 0) {
    const file = fileInput?.files[0];
    fileReader.onload = fileLoaded;
    fileReader.readAsDataURL(file);
  }
}

/**
 * Handler when file is loaded
 */
function fileLoaded() {
  image = new Image();
  image.onload = () => {
    const container = document.getElementById('container');
    if (container !== null) {
      const containerStyle = getComputedStyle(container);
      canvas.width = container.clientWidth - parseInt(containerStyle.paddingLeft.replace('px', '')) - parseInt(containerStyle.paddingRight.replace('px', ''));
      canvas.height = image.height / image.width * canvas.width;
      drawImage();
    }
  };
  image.src = fileReader?.result?.toString() ?? '';
}

/**
 * Draw image on canvas
 */
function drawImage() {
  context?.drawImage(image, 0, 0, image.width, image.height, 0, 0, canvas.width, canvas.height);
}

/**
 * Create a GIF
 */
function createGif() {
  const steps = parseInt((document.getElementById('steps') as HTMLInputElement).value);
  const delay = parseInt((document.getElementById('delay') as HTMLInputElement).value);

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const gif: any = new GIF({
    workers: 2,
    // quality: 10,
  });

  const scalingX = image.width / canvas.width;
  const scalingY = image.height / canvas.height;

  const stepXLeft = Math.ceil(target[0][0] / steps);
  // eslint-disable-next-line no-magic-numbers
  const stepXRight = Math.ceil((canvas.width - target[1][0]) / steps);
  // eslint-disable-next-line no-magic-numbers
  const stepYTop = Math.ceil(target[0][1] / steps);
  // eslint-disable-next-line no-magic-numbers
  const stepYBottom = Math.ceil((canvas.height - target[1][1]) / steps);

  for (let step = 0; step < steps; step++) {
    const xLeft = step * stepXLeft * scalingX;
    const yTop = step * stepYTop * scalingY;
    const xRight = image.width - step * stepXRight * scalingX;
    const yBottom = image.height - step * stepYBottom * scalingY;

    context?.drawImage(
      image,
      xLeft,
      yTop,
      xRight - xLeft,
      yBottom - yTop,
      0,
      0,
      canvas.width,
      canvas.height,
    );

    gif.addFrame(canvas, {
      copy: true,
      delay: delay,
    });
  }

  gif.on('progress', (progress: number) => {
    if (typeof progressBar?.style === 'object' && 'width' in progressBar?.style) {
      // eslint-disable-next-line no-magic-numbers
      progressBar.style.width = `${progress * 100}%`;
      // eslint-disable-next-line no-magic-numbers
      progressBar?.setAttribute('aria-valuenow', (progress * 100).toString());
    }
  });

  gif.on('finished', (blob: unknown) => {
    window.open(URL.createObjectURL(blob));
  });

  gif.render();
}
